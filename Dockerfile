
FROM golang:1.12 as builder
# Copy local code to the container image.
WORKDIR /go/src/backend
COPY . .

RUN go get -u github.com/go-sql-driver/mysql
RUN go get -u cloud.google.com/go/firestore
RUN go get -u google.golang.org/api/iterator

# Build the command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN CGO_ENABLED=0 GOOS=linux go build -v -o server

# Use a Docker multi-stage build to create a lean production image.
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
FROM alpine
RUN apk add --no-cache ca-certificates

# Copy the binary to the production image from the builder stage.
COPY --from=builder /go/src/backend/server /server

# Run the web service on container startup.
CMD ["/server"]