module github.com/atif-zia/fernando/grayll.io-firebase-project-grayll-system-test/backend

go 1.12

require (
	cloud.google.com/go/firestore v1.1.1
	google.golang.org/api v0.15.0
)
