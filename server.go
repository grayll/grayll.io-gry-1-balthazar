package main

import (
	"cloud.google.com/go/firestore"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	(*w).Header().Set("Content-Type", "application/json")
	//(*w).Header().Set("Content-Type", "application/x-www-form-urlencoded")
}

type Func1stParams struct {
	func1stUnit    string
	func1stP0      float64
	func1stRbi     float64
	func1stRMaxi   float64
	func1stTMaxi   float64
	func1stTMaxi0  float64
	func1stRbd     float64
	func1stRMaxD   float64
	func1stT0MaxD  float64
	func1stLogBase string
	func1stC       int
}

type Func2ndParams struct {
	func2ndLcs   float64
	func2ndMinCs float64
	func2ndRbi   float64
	func2ndC     float64
}

type Func3rdParams struct {
	func3rdLRs    float64
	func3rdMaxRs  float64
	func3rdMinRs  float64
	func3rdRMinTs float64
	func3rdRMaxTs float64
	func3rdC      float64
}

type Func4thParams struct {
	func4thLAs    float64
	func4thMaxAs  float64
	func4thMinAs  float64
	func4thRMinTs float64
	func4thRMaxTs float64
	func4thC      float64
}

type UserAlgoPosition struct {
	id         float64
	item       string
	usdValue   float64
	itemAmount float64
	token      string
	grxAmount  float64
}

type UserAlgoPositionFullData struct {
	StellarTxnId      string  `json:"StellarTxnId"`
	GrayllTxnId       string  `json:"GrayllTxnId"`
	GrxAmount         float64 `json:"GrxAmount"`
	GrxUsdRate        float64 `json:"GrxUsdRate"`
	Item              string  `json:"Item"`
	ItemAmount        float64 `json:"ItemAmount"`
	OpenTimeStamp     int64   `json:"OpenTimeStamp"`
	Roi               float64 `json:"Roi"`
	Status            int64   `json:"Status"`
	Token             string  `json:"Token"`
	UsdPositionProfit float64 `json:"UsdPositionProfit"`
	UsdPositionValue  float64 `json:"UsdPositionValue"`
	UsdValue          float64 `json:"UsdValue"`
	UserId            float64 `json:"UserId"`
}

func parseObjectStringToFunc1stParams(
	str string,
) (params Func1stParams) {
	subStr := strings.Replace(str, "{", "", -1)
	subStr = strings.Replace(subStr, "}", "", -1)
	subStr = strings.Replace(subStr, string('"'), "", -1)
	parsedStr := strings.Split(subStr, ",")
	for _, element := range parsedStr {
		parsedElement := strings.Split(element, ":")
		switch parsedElement[0] {
		case "func1stUnit":
			params.func1stUnit = parsedElement[1]
		case "func1stP0":
			params.func1stP0, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stRbi":
			params.func1stRbi, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stRMaxi":
			params.func1stRMaxi, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stTMaxi":
			params.func1stTMaxi, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stTMaxi0":
			params.func1stTMaxi0, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stRbd":
			params.func1stRbd, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stRMaxD":
			params.func1stRMaxD, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stT0MaxD":
			params.func1stT0MaxD, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func1stLogBase":
			params.func1stLogBase = parsedElement[1]
		case "func1stC":
			params.func1stC, _ = strconv.Atoi(parsedElement[1])
		}
	}
	return params
}

func parseObjectStringToFunc2ndParams(
	str string,
) (params Func2ndParams) {
	subStr := strings.Replace(str, "{", "", -1)
	subStr = strings.Replace(subStr, "}", "", -1)
	subStr = strings.Replace(subStr, string('"'), "", -1)
	parsedStr := strings.Split(subStr, ",")
	for _, element := range parsedStr {
		parsedElement := strings.Split(element, ":")
		switch parsedElement[0] {
		case "func2ndLcs":
			params.func2ndLcs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func2ndMinCs":
			params.func2ndMinCs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func2ndRbi":
			params.func2ndRbi, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func2ndC":
			params.func2ndC, _ = strconv.ParseFloat(parsedElement[1], 64)
		}
	}
	return params
}

func parseObjectStringToFunc3rdParams(
	str string,
) (params Func3rdParams) {
	subStr := strings.Replace(str, "{", "", -1)
	subStr = strings.Replace(subStr, "}", "", -1)
	subStr = strings.Replace(subStr, string('"'), "", -1)
	parsedStr := strings.Split(subStr, ",")
	for _, element := range parsedStr {
		parsedElement := strings.Split(element, ":")
		switch parsedElement[0] {
		case "func3rdLRs":
			params.func3rdLRs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func3rdMaxRs":
			params.func3rdMaxRs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func3rdMinRs":
			params.func3rdMinRs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func3rdRMinTs":
			params.func3rdRMinTs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func3rdRMaxTs":
			params.func3rdRMaxTs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func3rdC":
			params.func3rdC, _ = strconv.ParseFloat(parsedElement[1], 64)
		}
	}
	return params
}

func parseObjectStringToFunc4thParams(
	str string,
) (params Func4thParams) {
	subStr := strings.Replace(str, "{", "", -1)
	subStr = strings.Replace(subStr, "}", "", -1)
	subStr = strings.Replace(subStr, string('"'), "", -1)
	parsedStr := strings.Split(subStr, ",")
	for _, element := range parsedStr {
		parsedElement := strings.Split(element, ":")
		switch parsedElement[0] {
		case "func4thLAs":
			params.func4thLAs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func4thMaxAs":
			params.func4thMaxAs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func4thMinAs":
			params.func4thMinAs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func4thRMinTs":
			params.func4thRMinTs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func4thRMaxTs":
			params.func4thRMaxTs, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "func4thC":
			params.func4thC, _ = strconv.ParseFloat(parsedElement[1], 64)
		}
	}
	return params
}

func parseObjectStringToUserAlgoPosition(
	str string,
) (params UserAlgoPosition) {
	subStr := strings.Replace(str, "{", "", -1)
	subStr = strings.Replace(subStr, "}", "", -1)
	subStr = strings.Replace(subStr, string('"'), "", -1)
	parsedStr := strings.Split(subStr, ",")
	for _, element := range parsedStr {
		parsedElement := strings.Split(element, ":")
		switch parsedElement[0] {
		case "id":
			params.id, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "item":
			params.item = parsedElement[1]
		case "usdValue":
			params.usdValue, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "grxAmount":
			params.grxAmount, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "itemAmount":
			params.itemAmount, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "token":
			params.token = parsedElement[1]
		}
	}
	return params
}

func parseGetUserOpenPositionsParams(
	str string,
) (userId float64, start float64, count float64) {
	subStr := strings.Replace(str, "{", "", -1)
	subStr = strings.Replace(subStr, "}", "", -1)
	subStr = strings.Replace(subStr, string('"'), "", -1)
	parsedStr := strings.Split(subStr, ",")
	for _, element := range parsedStr {
		parsedElement := strings.Split(element, ":")
		switch parsedElement[0] {
		case "userId":
			userId, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "start":
			start, _ = strconv.ParseFloat(parsedElement[1], 64)
		case "count":
			count, _ = strconv.ParseFloat(parsedElement[1], 64)
		}
	}
	return userId, start, count
}

func recursionCountDigits(number int64) int64 {
	if number < 10 {
		return 1
	} else {
		return 1 + recursionCountDigits(number/10)
	}
}

func main() {
	http.HandleFunc("/func1st", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc1stParams(string(body))
		P, R := RunFunction1stAlgorithm(params)
		jsonDataP, _ := json.Marshal(P)
		jsonDataR, _ := json.Marshal(R)
		returnData := map[string]string{
			"res": "success",
			"P":   string(jsonDataP),
			"R":   string(jsonDataR),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/func2nd", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc2ndParams(string(body))
		var ROI []float64
		var AS []float64
		for i := 0; i <= 100; i++ {
			params.func2ndLcs = 100.0 - float64(i)
			result := RunFunction2ndAlgorithm(params)
			if result == -1.0 {
				ROI = append(ROI, -1)
				AS = append(AS, -1)
			} else {
				ROI = append(ROI, result)
				AS = append(AS, params.func2ndLcs)
			}
		}
		jsonDataROI, _ := json.Marshal(ROI)
		jsonDataAS, _ := json.Marshal(AS)
		returnData := map[string]string{
			"res": "success",
			"ROI": string(jsonDataROI),
			"AS":  string(jsonDataAS),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/func3rd", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc3rdParams(string(body))
		var RS []float64
		var ROI []float64
		for i := params.func3rdMaxRs; i >= params.func3rdMinRs; i-- {
			params.func3rdLRs = i
			result := RunFunction3rdAlgorithm(params)
			RS = append(RS, i)
			if result == 0 {
				ROI = append(ROI, 0)
			} else {
				ROI = append(ROI, result)
			}
		}
		jsonDataROI, _ := json.Marshal(ROI)
		jsonDataRS, _ := json.Marshal(RS)
		returnData := map[string]string{
			"res": "success",
			"ROI": string(jsonDataROI),
			"RS":  string(jsonDataRS),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/func4th", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc4thParams(string(body))
		var AS []float64
		var ROI []float64
		for i := params.func4thMaxAs; i >= params.func4thMinAs; i-- {
			params.func4thLAs = i
			result := RunFunction4thAlgorithm(params)
			AS = append(AS, i)
			if result == 0 {
				ROI = append(ROI, 0)
			} else {
				ROI = append(ROI, result)
			}
		}
		jsonDataROI, _ := json.Marshal(ROI)
		jsonDataAS, _ := json.Marshal(AS)
		returnData := map[string]string{
			"res": "success",
			"ROI": string(jsonDataROI),
			"AS":  string(jsonDataAS),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/funcMain4th", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc4thParams(string(body))
		result := RunFunction4thAlgorithm(params)
		returnData := map[string]string{
			"res": "success",
			"ROI": fmt.Sprintf("%f", result),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/funcMain3rd", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc3rdParams(string(body))
		result := RunFunction3rdAlgorithm(params)
		returnData := map[string]string{
			"res": "success",
			"ROI": fmt.Sprintf("%f", result),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/funcMain2nd", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToFunc2ndParams(string(body))
		result := RunFunction2ndAlgorithm(params)
		returnData := map[string]string{
			"res": "success",
			"ROC": fmt.Sprintf("%f", result),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	projectID := "grayll-gry-1-balthazar"
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	//defer client.Close()

	http.HandleFunc("/CreateNewUserPosition", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		params := parseObjectStringToUserAlgoPosition(string(body))
		iter := client.Collection("Environment").Documents(ctx)
		var lastGrayllTxnID int64
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				fmt.Println(err)
			}
			if doc.Data()["key"].(string) == "GrayllTxnID" {
				lastGrayllTxnID, _ = strconv.ParseInt(doc.Data()["value"].(string), 10, 64)
			}
		}
		newGrayllTxnID := lastGrayllTxnID + 1
		var newGrayllTxnIdString string
		newGrayllTxnIdDecimalCount := recursionCountDigits(newGrayllTxnID)
		if newGrayllTxnIdDecimalCount < 18 {
			diff := 18 - newGrayllTxnIdDecimalCount
			var i int64
			for i = 1; i <= diff; i++ {
				newGrayllTxnIdString = "0" + newGrayllTxnIdString
			}
		}
		newGrayllTxnIdString = newGrayllTxnIdString + strconv.FormatInt(int64(newGrayllTxnID), 10)
		openTimeStamp := time.Now().Unix()
		_, _, err = client.Collection("userPositions").Add(ctx, map[string]interface{}{
			"userId":            params.id,
			"openTimeStamp":     openTimeStamp,
			"status":            1,
			"item":              params.item,
			"grxAmount":         params.grxAmount,
			"usdValue":          params.usdValue,
			"usdPositionValue":  0.1,
			"usdPositionProfit": 0.1,
			"roi":               0.1,
			"grayllTxnId":       newGrayllTxnIdString,
			"stellarTxnId":      "",
			"grxUsdRate":        0.22,
			"itemAmount":        params.itemAmount,
			"token":             params.token,
		})
		if err != nil {
			log.Fatalf("Failed adding alovelace: %v", err)
		}
		returnData := map[string]string{
			"code":    strconv.Itoa(200),
			"status":  "success",
			"results": fmt.Sprintf("%f", params),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	http.HandleFunc("/GetUserOpenPositions", func(w http.ResponseWriter, r *http.Request) {
		setupResponse(&w, r)
		if (*r).Method == "OPTIONS" {
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatalln(err)
		}
		userId, _, _ := parseGetUserOpenPositionsParams(string(body))
		iter := client.Collection("userPositions").Where("userId", "==", userId).Documents(ctx)
		resultData := []UserAlgoPositionFullData{{}}
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				fmt.Println(err)
			}
			rowData := doc.Data()
			result := UserAlgoPositionFullData{
				StellarTxnId:      rowData["stellarTxnId"].(string),
				GrayllTxnId:       rowData["grayllTxnId"].(string),
				GrxAmount:         rowData["grxAmount"].(float64),
				GrxUsdRate:        rowData["grxUsdRate"].(float64),
				Item:              rowData["item"].(string),
				ItemAmount:        rowData["itemAmount"].(float64),
				OpenTimeStamp:     rowData["openTimeStamp"].(int64),
				Roi:               rowData["roi"].(float64),
				Status:            rowData["status"].(int64),
				Token:             rowData["token"].(string),
				UsdPositionProfit: rowData["usdPositionProfit"].(float64),
				UsdPositionValue:  rowData["usdPositionValue"].(float64),
				UsdValue:          rowData["usdValue"].(float64),
				UserId:            rowData["userId"].(float64),
			}
			resultData = append(resultData, result)
		}
		jsonResultData, _ := json.Marshal(resultData)
		returnData := map[string]string{
			"code":   strconv.Itoa(200),
			"status": "success",
			"result": string(jsonResultData),
		}
		jsonDataReturn, _ := json.Marshal(returnData)
		_, _ = w.Write(jsonDataReturn)
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

func RunFunction1stAlgorithm(params Func1stParams) ([]float64, []float64) {
	var P []float64
	var R []float64
	TMaxi0 := params.func1stTMaxi + params.func1stTMaxi0
	T0MaxD := TMaxi0 + params.func1stT0MaxD
	P = append(P, params.func1stP0)
	R = append(R, 0.00)
	T0 := 0.0
	T1 := T0 + 1.0
	floatC := float64(params.func1stC)
	k := 0
	var PD []float64
	PD = append(PD, params.func1stP0)
	PDCnt := 1
	for {
		k++
		T_k := T0 + float64(k)
		var temp_R_k float64
		var temp_P_k float64
		var expmValue float64
		if params.func1stLogBase == "e" {
			expmValue = math.Expm1(floatC)
		} else {
			expmValue = math.Pow(10, floatC) - 1
		}
		if T_k >= 1.0 && T_k <= params.func1stTMaxi {
			if params.func1stLogBase == "e" {
				temp_R_k = params.func1stRbi + (params.func1stRMaxi-params.func1stRbi)/floatC*math.Log(1.0+expmValue*(T_k-T1)/(params.func1stTMaxi-T1))
			} else {
				temp_R_k = params.func1stRbi + (params.func1stRMaxi-params.func1stRbi)/floatC*math.Log10(1.0+expmValue*(T_k-T1)/(params.func1stTMaxi-T1))
			}
		} else if T_k > params.func1stTMaxi && T_k <= TMaxi0 {
			if params.func1stLogBase == "e" {
				temp_R_k = params.func1stRMaxi * (1.0 - math.Log(1.0+expmValue*(T_k-params.func1stTMaxi)/(TMaxi0-params.func1stTMaxi))/floatC)
			} else {
				temp_R_k = params.func1stRMaxi * (1.0 - math.Log10(1.0+expmValue*(T_k-params.func1stTMaxi)/(TMaxi0-params.func1stTMaxi))/floatC)
			}
		} else if T_k > TMaxi0 && T_k <= T0MaxD {
			if params.func1stLogBase == "e" {
				temp_R_k = -(params.func1stRbd + ((params.func1stRMaxD-params.func1stRbd)/floatC)*math.Log(1.0+(expmValue*(T_k-TMaxi0-T1))/(T0MaxD-TMaxi0-T1)))
			} else {
				temp_R_k = -(params.func1stRbd + ((params.func1stRMaxD-params.func1stRbd)/floatC)*math.Log10(1.0+(expmValue*(T_k-TMaxi0-T1))/(T0MaxD-TMaxi0-T1)))
			}
		} else if T_k > T0MaxD {
			temp_R_k = R[k-1]
		}

		R = append(R, temp_R_k)
		if k > 0 {
			temp_P_k = PD[PDCnt-1] * (1.0 + R[k]/100.0)
		}
		if temp_P_k < 0.00045 {
			break
		} else {
			P = append(P, temp_P_k)
			//if params.func1stUnit == "Days" || params.func1stUnit == "Hours" && k%24 == 0 || params.func1stUnit == "Minutes" && k%1440 == 0 || params.func1stUnit == "Seconds" && k%86400 == 0 {
			//	if k > 0 {
			//		PD = append(PD, temp_P_k)
			//		PDCnt++
			//	}
			//}
			PD = append(PD, temp_P_k)
			PDCnt++
		}
	}
	return P, R
}

func RunFunction2ndAlgorithm(
	params Func2ndParams,
) float64 {
	C := int(params.func2ndC)
	status := true
	if params.func2ndLcs > 100.00 {
		status = false
	}
	if C == 0 {
		status = false
	}
	if params.func2ndMinCs < 0.00 || params.func2ndRbi < 0 {
		status = false
	}
	if params.func2ndLcs < params.func2ndMinCs {
		status = false
	}
	if status == true {
		var value float64
		if params.func2ndLcs == 100.00 {
			value = params.func2ndRbi
		} else {
			floatC := float64(C)
			value = params.func2ndRbi * (1 - 1/floatC*math.Log(1+math.Expm1(floatC)*(1-params.func2ndLcs/100.00)/(1-params.func2ndMinCs/100.00)))
		}
		return value
	} else {
		return 0
	}
}

func RunFunction3rdAlgorithm(
	params Func3rdParams,
) float64 {
	C := int(params.func3rdC)
	status := true
	if params.func3rdMinRs < 0.00 || params.func3rdMinRs > 100.00 || params.func3rdMaxRs < 0.00 || params.func3rdMaxRs > 100.00 {
		status = false
	}
	if params.func3rdMinRs > params.func3rdMaxRs {
		status = false
	}
	if C == 0 {
		status = false
	}
	if params.func3rdRMinTs < 0.00 || params.func3rdRMaxTs < 0 {
		status = false
	}
	if params.func3rdRMinTs > params.func3rdRMaxTs {
		status = false
	}
	if params.func3rdLRs < params.func3rdMinRs || params.func3rdLRs > params.func3rdMaxRs {
		status = false
	}
	if status == true {
		var ROI float64
		if params.func3rdLRs == params.func3rdMaxRs {
			ROI = params.func3rdRMinTs
		} else {
			ROI = params.func3rdRMinTs + (params.func3rdRMaxTs-params.func3rdRMinTs)*(math.Log(1+(math.Expm1(float64(C))*(params.func3rdMaxRs-params.func3rdLRs)/(params.func3rdMaxRs-params.func3rdMinRs)))/math.Log(math.Exp(float64(C))))
		}
		return ROI
	} else {
		return 0
	}
}

func RunFunction4thAlgorithm(
	params Func4thParams,
) float64 {
	C := params.func4thC
	status := true
	if params.func4thMinAs < 0.00 || params.func4thMinAs > 100.00 || params.func4thMaxAs < 0.00 || params.func4thMaxAs > 100.00 {
		status = false
	}
	if params.func4thMinAs > params.func4thMaxAs {
		status = false
	}
	if C == 0 {
		status = false
	}
	if params.func4thRMinTs < 0.00 || params.func4thRMaxTs < 0 {
		status = false
	}
	if params.func4thRMinTs > params.func4thRMaxTs {
		status = false
	}
	if params.func4thLAs < params.func4thMinAs || params.func4thLAs > params.func4thMaxAs {
		status = false
	}
	if status == true {
		var ROI float64
		if params.func4thLAs == params.func4thMaxAs {
			ROI = params.func4thRMinTs
		} else {
			ROI = params.func4thRMinTs + (params.func4thRMaxTs-params.func4thRMinTs)*(math.Log(1+(math.Expm1(float64(C))*(params.func4thMaxAs-params.func4thLAs)/(params.func4thMaxAs-params.func4thMinAs)))/math.Log(math.Exp(float64(C))))
		}
		return ROI
	} else {
		return 0
	}
}
